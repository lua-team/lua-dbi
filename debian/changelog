lua-dbi (0.7.4-1) unstable; urgency=medium

  * New upstream version 0.7.4
  * fix builds on 32bit systems

 -- Victor Seva <vseva@debian.org>  Tue, 08 Oct 2024 08:38:29 +0200

lua-dbi (0.7.3-1) unstable; urgency=medium

  * New upstream version 0.7.3
  * update Standards-Version, no changes needed

 -- Victor Seva <vseva@debian.org>  Mon, 20 May 2024 09:53:04 +0200

lua-dbi (0.7.2-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Use versioned copyright format URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository-Browse.
  * Drop transition for old debug package migration.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 10 Dec 2022 12:03:37 +0000

lua-dbi (0.7.2-3) unstable; urgency=medium

  * Lua team as maintainer, me as uploader (Closes: #995542)
  * watch to version 4
  * add Lua5.4 support

 -- Victor Seva <vseva@debian.org>  Mon, 27 Dec 2021 12:48:58 +0100

lua-dbi (0.7.2-2) unstable; urgency=medium

  * update standards-version to 4.4.1, no changes needed

 -- Victor Seva <vseva@debian.org>  Tue, 19 Nov 2019 09:24:45 +0100

lua-dbi (0.7.2-1) unstable; urgency=medium

  * New upstream version 0.7.2
  * remove already applied patches

 -- Victor Seva <vseva@debian.org>  Tue, 21 May 2019 12:35:55 +0200

lua-dbi (0.7.1-2) unstable; urgency=medium

  * add upstream patches for fix postgresql issues

 -- Victor Seva <vseva@debian.org>  Wed, 09 Jan 2019 16:29:47 +0100

lua-dbi (0.7.1-1) unstable; urgency=medium

  * remove already applied patches from upstream
  * New upstream version 0.7.1

 -- Victor Seva <vseva@debian.org>  Fri, 19 Oct 2018 16:16:44 +0200

lua-dbi (0.6-2) unstable; urgency=medium

  * fix coredump on postgresql (already applied upstream master)
  * fix capitalization-error-in-description lintian warnings

 -- Victor Seva <vseva@debian.org>  Tue, 06 Mar 2018 18:07:18 +0100

lua-dbi (0.6-1) unstable; urgency=medium

  * add myself to Uploaders. wrap-and-sort -sat
  * update Vcs-* migrated to salsa
  * update upstream
  * New upstream version 0.6
  * update compat to 10, Standards-Version to 4.1.3 and remove dbg packages
  * add support for lua5.2 and lua5.3
  * remove remove-shebang.patch (already applied upstream)
  * add patches fixing warnings (already applied in upstream master)
  * update LUA_MODNAME(_CPART)

 -- Victor Seva <vseva@debian.org>  Mon, 05 Mar 2018 17:25:16 +0100

lua-dbi (0.5.hg5ba1dd988961-4) unstable; urgency=medium

  * Team upload
  * Replace Build-Depends: libmysqlclient-dev by default-libmysqlclient-dev
    (Closes: #845871)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 16 Dec 2016 04:46:14 +0100

lua-dbi (0.5.hg5ba1dd988961-3) unstable; urgency=medium

  * Do not pre-depend on multiarch-support

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 15 Aug 2015 15:56:07 +0200

lua-dbi (0.5.hg5ba1dd988961-2) unstable; urgency=low

  * Build-depend on postgresql-server-dev-all

 -- Enrico Tassi <gareuselesinge@debian.org>  Wed, 18 Sep 2013 10:39:16 +0200

lua-dbi (0.5.hg5ba1dd988961-1) unstable; urgency=low

  * New upstream snapshot fixing mysql bug
  * Packaging moved to git

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 10 Aug 2013 20:35:27 +0200

lua-dbi (0.5+svn78-5~dbg4matthew1) unstable; urgency=low

  * debian/compat set to 9
  * new package lua-dbi-mysql-dbg
  * new package lua-dbi-sqlite3-dbg
  * new package lua-dbi-postgresql-dbg

 -- Enrico Tassi <gareuselesinge@debian.org>  Sat, 30 Jun 2012 11:22:38 +0200

lua-dbi (0.5+svn78-4) unstable; urgency=low

  * Add patch preventing an off by one in mysql driver:
      http://code.matthewwild.co.uk/luadbi/rev/5ba1dd988961

 -- Enrico Tassi <gareuselesinge@debian.org>  Fri, 29 Jun 2012 19:00:58 +0200

lua-dbi (0.5+svn78-3) unstable; urgency=low

  * Add path: mysql-memory-consumption from
      http://code.matthewwild.co.uk/luadbi/rev/7c968f66bccd

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 30 Apr 2012 16:22:07 +0200

lua-dbi (0.5+svn78-2) unstable; urgency=low

  * Ship -dev packages

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 09 Apr 2012 15:04:16 +0200

lua-dbi (0.5+svn78-1) unstable; urgency=low

  * Initial release. (Closes: #668173)

 -- Enrico Tassi <gareuselesinge@debian.org>  Mon, 09 Apr 2012 14:50:23 +0200
